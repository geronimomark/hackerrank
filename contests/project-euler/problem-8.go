package main

import (
    "fmt"
    "strconv"
    "strings"
)

func main() {
 //Enter your code here. Read input from STDIN. Print output to STDOUT
    var ctr, numLen, numConsecutive int
    var number string
    fmt.Scanf("%d", &ctr)
    
    for ; ctr > 0; ctr-- {
        fmt.Scanf("%d", &numLen)
        fmt.Scanf("%d", &numConsecutive)
        fmt.Scanf("%s", &number)
                
        var pArr []int
        x := numConsecutive-1
        
        for i := 0; i+x < numLen; i++ {
            str2 := number[i:i+x+1]
            sArr2 := strings.Split(str2, "")
            prod := 1
            for _, v := range sArr2 {
                n, _ := strconv.Atoi(v)
                prod = prod * n
            }
            if prod > 0 {
                pArr = append(pArr, prod)
            }
        }

        highest := 0
        
        for _, v := range pArr {
            if highest < v {
                highest = v
            }
        }
        
        fmt.Println(highest)
    }
    
}
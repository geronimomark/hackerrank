<?php
$_fp = fopen("php://stdin", "r");
/* Enter your code here. Read input from STDIN. Print output to STDOUT */

while ($data = fgets($_fp)) {
    $arr[] = $data + 0;
}

foreach ($arr as $k => $val) {
    if ($k == 0) continue;
    $sum = 0;
    $flag = false;
    
    $i = 1;
    while (! $flag) {
        $prod = $val * $i;
        
        for ($ctr = 1; $ctr <= $val; $ctr++) {
            if ($prod % $ctr == 0) {
                $flag = true;
            } else {
                $flag = false;
                break;
            }
        }
        $i++;
    }
    print $prod . PHP_EOL;
}


?>
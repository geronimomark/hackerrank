package main
import "fmt"
import "bufio"
import "strconv"
import "os"

func main() {
 //Enter your code here. Read input from STDIN. Print output to STDOUT
    scn := bufio.NewScanner(os.Stdin)
    var sum int64
    var exitcount int
    var firstflag int = 0
    for scn.Scan() {
        firstflag++
        sum = 0
        if firstflag == 1 {
            exitcount, _ = strconv.Atoi(scn.Text())
            continue
        }
        exitcount--
        if exitcount < 0 {
            break
        }
        x, y := 1, 2
        number, _ := strconv.Atoi(scn.Text())
        for x <= number {
            if x % 2 == 0 {
                sum = sum + int64(x)
            }
            z := x + y
            x = y
            y = z
        }
        fmt.Println(sum)
    }
}
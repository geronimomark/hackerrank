<?php
$_fp = fopen("php://stdin", "r");
/* Enter your code here. Read input from STDIN. Print output to STDOUT */
$flag = 1;
while($data = fgets($_fp)) {
    $data += 0;
    if ($flag == 1) {
        $flag++;
        continue;
    }
    
    $sum1 = 0; $sum2 = 0;
    
    for ($i = 1; $i <= $data; $i++) {
        $sum1 += $i * $i;
        $sum2 += $i;
    }
    
    print ($sum2 * $sum2) - $sum1 . PHP_EOL;
}

?>
<?php
$_fp = fopen("php://stdin", "r");
/* Enter your code here. Read input from STDIN. Print output to STDOUT */
$data = fread($_fp, 8192);
$arr = explode("\n", $data);

$pal = []; $i = 1;
for ($factor1 = 100; $factor1 <= 999; $factor1++) {
    for ($factor2 = 100; $factor2 <= 999; $factor2++) {
        $prod = $factor1 * $factor2;
        //if ($prod > $arr[$i]) {$i++; break;}
        $reversenum = strrev($prod."");
        if($prod.""==$reversenum && !in_array($prod, $pal)) {
            $pal[] = $prod;
        }
    }
}

asort($pal);

foreach ($arr as $key => $val) {
    $a = 0;
    if ($key == 0) continue;
    foreach ($pal as $val2) {
        if ($val2 >= $val) {
            break;
        } else {
            $a = $val2;
        }
    }
    print $a . PHP_EOL;
}


?>
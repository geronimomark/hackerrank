package main
import "fmt"

func main() {
 //Enter your code here. Read input from STDIN. Print output to STDOUT
    var ctr int
    var num int
    var prime []int 
    prime = []int{2,3,5,7,11,13,17}
    fmt.Scanf("%d", &ctr)
    
    for ctr > 0 {
        fmt.Scanf("%d", &num)
        length := len(prime)
        if (length >= num) {
            fmt.Println(prime[num-1])
        } else {
            top := prime[length-1] + 2
            
            for {
                isPrime := false
                ctr2 := 0
                for ctr2 <= length-1 {
                    if top % prime[ctr2] == 0 {
                        isPrime = false
                        break
                    } else {
                        isPrime = true
                    }
                    ctr2++
                }
                if isPrime {
                    prime = append(prime, top)
                    length = len(prime)        
                    if length == num {
                        break
                    }
                }
                top = top + 2
            }
            fmt.Println(prime[length-1])
        }
        ctr--
    }
    

}
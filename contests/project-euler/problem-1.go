package main
import "fmt"

func main() {
 //Enter your code here. Read input from STDIN. Print output to STDOUT
    var sum int64
    var number int64
    ctr := 1
    
    _, err := fmt.Scanf("%d", &number)
    
    for err == nil {
        if ctr == 1 {
            ctr++
            _, err = fmt.Scanf("%d", &number)
            continue
        }
               
        p := (number - 1) / 3
        sum = int64((3 * p * (p + 1)) / 2)
        
        q := (number - 1) / 5
        sum = sum + int64(((5 * q * (q + 1)) / 2))
        
        r := (number - 1) / 15
        sum = sum - int64(((15 * r * (r + 1)) / 2))

        fmt.Println(sum)
        
        _, err = fmt.Scanf("%d", &number)
    }
}